package main

import (
	"app/controllers"
	"app/models"
	"app/repositories"

	"github.com/gin-gonic/gin"
)

func main() {
	model := models.NewInventoryModel()
	repository := repositories.NewInventoryRepository(model)
	controller := controllers.NewInventoryController(repository)

	router := gin.Default()
	router.DELETE("/api/v1/inventory/:product", controller.DeleteInventory)
	router.Run()
}
